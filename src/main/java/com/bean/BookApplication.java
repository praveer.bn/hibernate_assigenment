package com.bean;

import com.dao.BookDaoImplementation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class BookApplication {
      BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
boolean flag=true;

    public void menu() throws IOException {

//
//        configuration  steps
//
//
//


//        creating object
//        Book book=new Book();
//        book.setBook_name("Marvel Comics:Spider Man");
//        book.setAuthor_name("Stan lee");
//        book.setBook_price(1001);
////        mapping with relation
//
//        session.save(book);
//        System.out.println("adding is done......");
//
//        transaction.commit();
//        session.close();
//        sessionFactory.close();


//        *******************************************************************************





        while (flag) {
            System.out.println("enter 1 for insert");
            System.out.println("enter 2 for update");
            System.out.println("enter 3 for delete");
            System.out.println("enter 4 for fetch");
            System.out.println("enter 5 for fetch by price criteria");
            System.out.println("enter 6 for fetch order by prize");
            System.out.println("enter 7 for projection");
            System.out.println("enter 8 for combination");
            System.out.println("enter 0 for exit");

            System.out.println("enter your option");
            int option = Integer.parseInt(bufferedReader.readLine());
            switch (option) {
                case 0:
                    System.out.println("thank you for using a application");
                    flag=false;
                    break;
                case 1:
                    System.out.println("enter the book details ==>book name ,author name and price ");
                    Book b= new Book();

                    b.setBookname(bufferedReader.readLine());
                    b.setAuthorname(bufferedReader.readLine());
                    b.setBookprice(Integer.parseInt(bufferedReader.readLine()));

                    int i = new BookDaoImplementation().insertBook(b);
                    if (i > 0) {

                        System.out.println("Records are adding successfully");
                    } else {
                        System.out.println("try again");
                    }

                    break;

                case 2:
                    System.out.println("enter the book details ,book name ,new  price ");

                    String bn = bufferedReader.readLine();
                    float bp=Float.parseFloat(bufferedReader.readLine());

                    int j=new BookDaoImplementation().updateBook(bn,bp);
                    if (j > 0) {
                        System.out.println("Records are adding successfully");
                    } else {
                        System.out.println("try again");
                    }
                    break;
                case 3:
                    System.out.println("enter the book details ==>book id");


                   int id=Integer.parseInt(bufferedReader.readLine());

                    int k=new BookDaoImplementation().deleteBook(id);
                    if (k > 0) {
                        System.out.println("Records are adding successfully");
                    } else {
                        System.out.println("try again");
                    }
                    break;
                case 4:
                    List<Book> list=new BookDaoImplementation().fetchBook();
                    list.stream().forEach(System.out::println);
                    break;
                case 5:
                    System.out.println("enter the price of restriction");
                    float p=Float.parseFloat(bufferedReader.readLine());
                    List<Book> l=new BookDaoImplementation().fetchByCriteria(p);
                    l.stream().forEach(System.out::println);
                    break;
//                    one for order===fetch book by desc order as per price
//                one for projection===select book and price and fetch details
//                one for order,projection ,restriction===select book name,auther name and price where
//                price >= 400 and order is asc as per price
                case 6:
                    List<Book> l1=new BookDaoImplementation().fetchByOrder();
                    l1.stream().forEach(System.out::println);
                    break;
                case 7:
                    new BookDaoImplementation().projectionUsed();
                    break;

                case 8:
                    new BookDaoImplementation().Combination();
                    break;


            }
        }
    }

    public static void main(String[] args) throws IOException {
        new BookApplication().menu();
    }
}
