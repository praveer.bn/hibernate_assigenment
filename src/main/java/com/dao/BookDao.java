package com.dao;

import com.bean.Book;

import java.util.List;

interface BookDao {
    int insertBook(Book b);
    int updateBook(String bn,float bp);
    int deleteBook(int id);
    List<Book> fetchBook();
    List<Book> fetchByCriteria(float p);
    List<Book> fetchByOrder();
    void projectionUsed();
    void Combination();
}
