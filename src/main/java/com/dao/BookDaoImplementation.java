package com.dao;

import com.bean.Book;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.*;
import org.hibernate.query.Query;


import javax.persistence.criteria.CriteriaBuilder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class BookDaoImplementation implements BookDao{
     static Configuration configuration;
     static SessionFactory sessionFactory;

     static{
         configuration=new Configuration().configure();
         sessionFactory=configuration.buildSessionFactory();
     }
    @Override
    public int insertBook(Book b) {
        Session session=sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        int i=(int) session.save(b);
        if (i>0){
            transaction.commit();
            System.out.println("Records are adding sucessfully");
        }else{
            System.out.println("try again");
        }
        return i;
    }

    @Override
    public int updateBook(String bn, float bp) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        String sql = "update Book SET bookprice = :x where bookname = :y ";
        Query query = session.createQuery(sql);
        query.setParameter("x", bp);
        query.setParameter("y", bn);

        int i = query.executeUpdate();
        System.out.println(bn+"===="+bp);
        if (i > 0) {
            System.out.println("updated");
            transaction.commit();
            session.close();
        } else {
            System.out.println("try again");
        }

        //session.close();
        return i;
    }
    @Override
    public int deleteBook(int id) {
        Session session=sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        Query query=session.createQuery("delete from Book where bookid = :bid");
        query.setParameter("bid",id);
       int i= query.executeUpdate();
       if(i>0){
           System.out.println("deleted");
           transaction.commit();
           session.close();
       }
       else {
           System.out.println("try again");
       }

        return i;
    }

    @Override
    public List<Book> fetchBook() {
        Session session=sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        Query query=session.createQuery("from Book");

        List<Book> list=query.list();

         return list;
    }

    @Override
    public List<Book> fetchByCriteria(float p) {
        Session session=configuration.buildSessionFactory().openSession();
        Criteria criteria=session.createCriteria(Book.class);
        criteria.add(Restrictions.gt("bookprice",p));
        return  criteria.list();
    }

    @Override
    public List<Book> fetchByOrder() {
        Session session=configuration.buildSessionFactory().openSession();
        Criteria criteria=session.createCriteria(Book.class);
        criteria.addOrder(Order.desc("bookprice"));
        return  criteria.list();

    }

    @Override
    public void projectionUsed() {
        Session session = configuration.buildSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Book.class);
        Projection projection1 = Projections.property("bookname");//list of bookname
        Projection projection = Projections.property("bookprice");//list of book price
        ProjectionList projectionList = Projections.projectionList();//hold list of both
        projectionList.add(projection);
        projectionList.add(projection1);
//
//        for(int i=0;i<projectionList.getLength();i++)
//        {
//            criteria.setProjection(projectionList.getProjection(i));
//
//            System.out.println(criteria.list());
//        }


//        List list=criteria.list();
//        for (int i=0;i<list.size();i++)
//        {
//            Object[] objects=(Object[])list.get(i);
//            System.out.println(objects[i]);
//        }

        criteria.setProjection(projectionList);
        List<Book> list = criteria.list();
        Iterator it = list.iterator();

        while (it.hasNext()) {

            Object ob[] = (Object[]) it.next();
            for (int i = 0; i < ob.length; i++) {
                System.out.print(ob[i] + " == ");
            }
            System.out.println();
        }
    }

    @Override
    public void Combination() {
        Session session=configuration.buildSessionFactory().openSession();
        Criteria criteria=session.createCriteria(Book.class);
        Projection projection01=Projections.property("bookname");//list of bookname
        Projection projection00=Projections.property("bookprice");//list of book price
        Projection projection02=Projections.property("authorname");
        ProjectionList projectionList1=Projections.projectionList();//hold list of both
        projectionList1.add(projection00);
        projectionList1.add(projection01);
        projectionList1.add(projection02);
        criteria.setProjection(projectionList1);
        criteria.add(Restrictions.gt("bookprice",400.0f)).addOrder(Order.asc("bookprice"));

        List<Book> list=criteria.list();
        Iterator i=list.iterator();
        while(i.hasNext())
        {
            Object ob[]=(Object[])i.next();
            for (int j = 0; j < ob.length; j++) {
                System.out.print(ob[j]+"==>");
            }
            System.out.println();
        }
    }
}
